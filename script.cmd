REM Part 1 for 3 dimension reduce
C:\Python27\python.exe ./part1.py ./Part1-dataset/ 3



REM Part 2 MAP calculation for three feature with 10 retrieval
C:\Python27\python.exe ./part2.py MAP ./Part2-dataset/ color_hist 10

C:\Python27\python.exe ./part2.py MAP ./Part2-dataset/ gabor 10

C:\Python27\python.exe ./part2.py MAP ./Part2-dataset/ canny 10



REM Part 2 Image Retrieval for three feature with query.png
C:\Python27\python.exe ./part2.py QUERY ./Part2-dataset/ ./query.png canny

C:\Python27\python.exe ./part2.py QUERY ./Part2-dataset/ ./query.png gabor

C:\Python27\python.exe ./part2.py QUERY ./Part2-dataset/ ./query.png color_hist