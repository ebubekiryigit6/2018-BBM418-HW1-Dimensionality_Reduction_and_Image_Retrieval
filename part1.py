import os
import sys
from PIL import Image
import numpy as np
from numpy import linalg as algebra
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

import util


def getDataSetFilesFromDirectory(dataset_path):
    """
    :param dataset_path: the parent directory of the image files to be read
    @:return: numpy array of pillow image object (image arrays have one dimension)
    """
    filePathList = [os.path.join(dataset_path, f) for f in os.listdir(dataset_path)]
    return [np.asarray(Image.open(path)).flatten() for path in filePathList]


def sortWithSameIndexes(toBeSort, toBeSortByOther):
    """
    :param toBeSort: the actual list to sort
    :param toBeSortByOther: list to be sorted by other list index
    :return: tuple that contains sorted lists
    """
    ziplist = zip(toBeSort, toBeSortByOther)
    sortedList = zip(*sorted(ziplist, reverse=True))
    return sortedList[0], sortedList[1]


def plotNDimensional(N, result_list):
    """
    Plots N dimensional results

    :param N: num of dimension
    :param result_list: reduced image list
    """
    fig = plt.figure()
    text_array = [i for i in range(1, len(result_list[0]) + 1)]

    util.writeLog("PLOT", "Plotting reduced images, dimension: " + str(N))

    if N == 1:
        ax = fig.add_subplot(111)
        ax.plot(result_list[0], np.zeros_like(result_list[0]), 'o')
        for x, y, text in zip(result_list[0], np.zeros_like(result_list[0]), text_array):
            ax.text(x, y, text)
        plt.show()

    elif N == 2:
        ax = fig.add_subplot(111)

        for text, x, y in zip(text_array, (result_list[0]), (result_list[1])):
            ax.text(x, y, text)

        ax.scatter(result_list[0], result_list[1], marker='d', c='r')
        plt.show()

    elif N == 3:
        ax = fig.add_subplot(111, projection=Axes3D.name)

        for text, x, y, z in zip(text_array, (result_list[0]), (result_list[1]), (result_list[2])):
            ax.text(x, y, z, text)

        ax.scatter(result_list[0], result_list[1], result_list[2], marker='d', c='r')
        plt.show()

    else:
        util.writeLog("PLOT", "Can't plot more than 3 dimensional.")
        print("Can't plot more than 3 dimensional.")


def reduce_dim(dataset_path, N_dimensional):
    """
    :param N_dimensional: num of dimensional that reduced images
    :param dataset_path:  the directory of the image files to be reduced dimension

    Draws a 3D chart that shows reduced dimension of images
    """
    # actual matrix 65536x11
    M = np.transpose(getDataSetFilesFromDirectory(dataset_path))

    # normalized matrix
    D = np.subtract(M, np.mean(M, axis=0))

    # covariance matrix 11x11
    Cov = np.matmul(np.transpose(M), M)

    # sorted eigenvalues and eigenvectors
    eigenvalues, eigenvectors = algebra.eig(Cov)
    eigenvalues, eigenvectors = sortWithSameIndexes(eigenvalues, eigenvectors)

    # images that has lower dimensional space
    result = np.matmul(eigenvectors[:N_dimensional], np.transpose(D))
    result = np.matmul(result, D)

    util.writeLog("REDUCE", "All image dimensions are reduced.")

    # plot the result
    plotNDimensional(N_dimensional, result)


def main(argv):
    util.writeLog("MAIN-PROCESS", "PCA program is started.")
    util.handlePart1Arguments(argv)
    reduce_dim(argv[1], int(argv[2]))
    util.writeLog("MAIN-PROCESS", "PCA program is completed successfully.")


if __name__ == "__main__":
    main(sys.argv)
