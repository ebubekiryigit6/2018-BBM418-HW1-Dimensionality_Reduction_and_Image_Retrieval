# Dimensionality Reduction and Image Retrieval

BBM418 - Computer Vision Lab.

Assignment 1

### Author
Ebubekir Yiğit - 21328629



### Project Introduction
The project consists of two main operations. 
The "part1.py" file reduces the size of the images 
in the given directory to N dimensions with the 
dimension reduction method PCA and draws the graph. 
The "part2.py" file trains classes and images in the given directory. 
The query image then prints the most similar images. 
It also tests the trained pictures with the "MAP" function and 
prints the accuracy percentage.


## Getting Started

You can browse the "script" files to run the entire process of the project.

I can not submit because the Submit system does not support cmd and sh files :)

For Windows:
```
    > script.cmd
```

For Linux:
```
    > sh script.sh
```


**All operations are progressively saved to the console.log file. 
You can browse the console.log file to see the operations.**




### Prerequisites

Python 2.7 is required to run the project. 

Python External Libraries:

    Pillow - Numpy - Matplotlib - OpenCV 
    
If you are having trouble installing OPENCV on Microsoft Windows:

-   Note that your "pip" version is 9.0 and above.
-   In the "C:\Python27\Scripts" directory you can run "pip install opencv-python".



### Running the Project

    python2.7 part1.py <dataset_path> <num_of_dimension>
    
    python2.7 part2.py MAP <dataset_path> <feature_method> <num_of_retrieval>
    
    python2.7 part2.py QUERY <dataset_path> <query_image_path> <feature_method>



#### USAGE

To Show PCA with 3 dimension:
```
python2.7 part1.py /path/of/dataset/ 3
```
-   Output: Plot with 3 dimension reduced images



To Image retrieval with query image:

    python2.7 part2.py QUERY /path/of/dataset/ ./query_image.jpg canny
    
-   Feature Methods: color_hist, gabor, canny (Other feature)

-   Output: Prints name of image files in dataset path ordered by similarity



To Calculate MAP:

    python2.7 part2.py MAP /path/of/dataset/ canny 10

-   Please put QUERY_IMAGES folder to dataset folder. (like sample Part2-dataset)

-   Feature Methods: color_hist, gabor, canny (Other feature)

-   Output: Calculated MAP percent. (Example: 0.36 -> %36)


**After run, please see "console.log" file to show process logs.**

