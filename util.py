import time
import pickle

import constants


def usagePart2():
    """
    prints USAGE and exits.
    """
    print("USAGE:\n")
    print("\tSELECT ONE:")
    print ("\t\tpart2.py QUERY <dataset_path> <query_image_path> <feature_method>")
    print ("\t\tpart2.py MAP <dataset_path> <feature_method> <N_Retrieve>")
    exit(1)


def usagePart1():
    """
    prints USAGE and exits.
    """
    print ("USAGE:\n\tpart1.py <dataset_path> <num_of_dimension>")
    exit(1)


def handlePart1Arguments(args):
    """
    Checks parameter len, if len is not acceptable prints usage.

    :param args: program arguments
    """
    if len(args) != 3:
        usagePart1()


def handlePart2Arguments(args):
    """
    Checks parameter len, if len is not acceptable prints usage.

    :param args: program arguments
    """
    if len(args) != 5:
        usagePart2()


def writeLog(tag, log):
    """
    Writes logs to console log file.

    :param tag: log tag
    :param log: log message
    """
    f = open(constants.LOG_FILE, "a")
    start = time.strftime("%H:%M:%S") + "   LOG/ " + tag.upper() + ":  "
    log = log.replace("\n", " " * len(start))
    buffer = start + log + "\n"
    f.write(buffer)
    f.close()


def writeAsSerializable(array, fileName):
    """
    Writes array to file as serializable.

    :param array: array to dump
    :param fileName: which file?
    """
    file = open(fileName, 'wb')
    pickle.dump(array, file)
    file.close()
    writeLog("PICKLE", "PICKLE file is created as " + fileName)


def getArrayFromPickle(fileName):
    """
    Gets array from pickle file.

    :param fileName: file that contains binary values of array
    :return: an array from given file
    """
    file = open(fileName, 'rb')
    array = pickle.load(file)
    file.close()
    writeLog("PICKLE", "Array is get from pickle file " + fileName)
    return array
