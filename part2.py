import sys
import numpy as np
import os
from cv2 import cv2
import ntpath

import constants
import util

RGB_FEATURE = "color_hist"
GABOR_FEATURE = "gabor"
MY_OWN_FEATURE = "canny"

# train image lists
imageNameList = []
imageList = []

# QUERY_IMAGES folder
queryImageList = []
queryImageNameList = []

# (train_set X feature_set) matrix
images_histogram_matrix = []


def getDistanceIndexes(query_image_hist, N):
    """
    calculates min distance of histogram values

    :param query_image_hist: histogram value of given query image
    :param N: num of distances that will return
    :return: images paths which are closest according to feature
    """
    result = np.array(images_histogram_matrix) - np.array(query_image_hist)
    result = np.power(result, 2)  # a^2
    result = np.sum(result, axis=1)  # a^2 + b^2 + c^2 + .....
    result = np.sqrt(result)  # distance value

    if N == -1:
        indexes = np.argsort(result)
    else:
        indexes = np.argsort(result)[0:N]

    return np.array(imageNameList)[indexes]


def createRgbHistogram(image):
    """
    Creates an array that has RGB intensity values
    Note: Don't use np.zeroes(768) it is very slow.

    :param image: RGB image
    :return: array of RGB features
    """
    height, width, channel = image.shape
    red = np.zeros(256)
    green = np.zeros(256)
    blue = np.zeros(256)

    for i in range(height):
        for j in range(width):
            red[image[i, j, 2]] += 1
            green[image[i, j, 1]] += 1
            blue[image[i, j, 0]] += 1
    return np.concatenate((blue, green, red))  # called by BGR


def __getGaborFilters(howMany):
    """
    Generates Gabor kernel array according to theta value.
    Example: if we want 4 gabor filters, it seems like:
     |  zero degree gabor filter
     /  45 degree
     -  90 degree
     \  135 degree

    :param howMany: number of gabor filters
    :return: array of gabor kernels
    """
    kernels = []
    size = constants.GABOR_FILTER_SIZE  # i think 31 is optimum value
    for theta in np.arange(0, np.pi, np.pi / howMany):
        kernel = cv2.getGaborKernel(
            (size, size),
            constants.SIGMA,
            theta,
            constants.LAMBD,
            constants.GAMMA,
            constants.PSI)
        kernels.append(kernel)
    return kernels


def createGaborHistogram(image):
    """
    Firstly converts image to grey-scale.
    Then filters it with Gabor filters.

    :param image: any image
    :return: array of Gabor features
    """
    grey_scaled_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    gabor_feature_list = []
    # get gabor filters
    for filter in __getGaborFilters(constants.NUMBER_OF_GABOR_FILTERS):
        # apply gabor filter and concat feature array
        gabor_image = cv2.filter2D(grey_scaled_image, cv2.CV_8UC3, filter)
        feature = np.zeros(256)
        height, width = gabor_image.shape
        for i in range(height):
            for j in range(width):
                feature[gabor_image[i, j]] += 1
        gabor_feature_list = np.concatenate((gabor_feature_list, feature))
    return gabor_feature_list


def createOwnFeatureHistogram(image):
    """
    This is my own feature histogram.
    Converts to grey-scale and applies Canny Edge detector.
    It generates histogram according to brightness and edge intensity values.
    So i think it will more efficient.

    :param image: any image
    :return: grey-scaled and Canny Edge detector feature histogram
    """
    grey_feature = np.zeros(256)
    canny_edge_feature = np.zeros(256)

    # apply grey scale
    grey_scaled_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    height, width = grey_scaled_image.shape
    for i in range(height):
        for j in range(width):
            grey_feature[grey_scaled_image[i, j]] += 1

    # calculate median value
    median = np.median(image)

    # I tried different threshold values but
    # single threshold with median value is more efficient
    # Because we want clear edge values
    edge = cv2.Canny(grey_scaled_image, median, median)
    height, width = edge.shape
    for i in range(height):
        for j in range(width):
            canny_edge_feature[edge[i, j]] += 1

    return np.concatenate((grey_feature, canny_edge_feature))


def rgb_feature():
    """
    creates RGB histogram matrix
    """
    for image in imageList:
        rgb = createRgbHistogram(image)
        images_histogram_matrix.append(rgb)
    util.writeLog("RGB-FEATURE", "RGB Feature matrix is populated.")


def gabor_feature():
    """
    creates Gabor Filter Bank histogram matrix
    """
    for image in imageList:
        gabor = createGaborHistogram(image)
        images_histogram_matrix.append(gabor)
    util.writeLog("GABOR-FEATURE", "Gabor Feature matrix is populated.")


def my_own_feature():
    """
    creates Grey-Scaled Canny Edge Detector histogram matrix
    """
    for image in imageList:
        own = createOwnFeatureHistogram(image)
        images_histogram_matrix.append(own)
    util.writeLog("OWN-FEATURE", "Canny Edge Detector Feature Matrix is populated.")


def populateImages(path):
    """
    Creates train and query images and their paths.

    :param path: dataset path
    """
    for f in os.listdir(path):
        if f != constants.QUERY_IMAGE_FOLDER_PATH:
            classPath = os.path.join(path, f)
            for imagePath in os.listdir(classPath):
                absolutePath = os.path.join(classPath, imagePath)
                imageNameList.append(absolutePath)
                imageList.append(cv2.imread(absolutePath))
        else:
            query_path = os.path.join(path, f)
            for query in os.listdir(query_path):
                absolutePath = os.path.join(query_path, query)
                queryImageNameList.append(absolutePath)
                queryImageList.append(cv2.imread(absolutePath))
    util.writeLog("INDEX-IMAGES", "Train set images and Query images are indexed.")


def calculateQueryHistogramAndDistances(query_image, feature_method, N, showLog):
    """
    Calculates min distance of histograms and
    gets images in train set which has min distance to query

    :param query_image: any image
    :param feature_method: RGB, GABOR or YOURS (my own feature)
    :param N: num of images
    :param showLog: print or not
    :return: sorted path of images
    """
    if feature_method == RGB_FEATURE:
        query_hist = createRgbHistogram(query_image)
    elif feature_method == GABOR_FEATURE:
        query_hist = createGaborHistogram(query_image)
    elif feature_method == MY_OWN_FEATURE:
        query_hist = createOwnFeatureHistogram(query_image)

    shortestDistancePathList = getDistanceIndexes(query_hist, N)
    if showLog:
        for path in shortestDistancePathList:
            print(path)
    return shortestDistancePathList


def featureFactory(dataset_path, feature_method):
    """
    Creates feature histogram matrix according to feature_method

    :param dataset_path: dataset path
    :param feature_method: RGB, GABOR or YOURS (my own feature)
    """
    populateImages(dataset_path)

    if feature_method == RGB_FEATURE:
        rgb_feature()
    elif feature_method == GABOR_FEATURE:
        gabor_feature()
    elif feature_method == MY_OWN_FEATURE:
        my_own_feature()
    else:
        util.writeLog("MAIN-PROCESS", "WRONG Feature Method: " + feature_method)
        print "Undefined feature method you must select one:", "\n\t" + RGB_FEATURE, "\n\t" + GABOR_FEATURE, "\n\t" + MY_OWN_FEATURE
        exit(1)


def __getIdFromFilePath(file_path, isQuery):
    """
    Gets id of image classes. Example:
    airplane_251_0175.jpg (Query) -> '251'
    251_0176.jpg (Train)          -> '251'

    :param file_path: file path
    :param isQuery: query image or train set image
    :return: class id
    """
    if isQuery:
        return ntpath.basename(file_path).split("_")[1]
    else:
        return ntpath.basename(file_path).split("_")[0]


def calculateMap(feature_method, N_Retrieval):
    """
    Calculates MAP value from QUERY_IMAGES folder.

    :param feature_method: RGB, GABOR or YOURS (my own feature)
    :param N_Retrieval: num of distance value
    :return:
    """
    MAP = []
    for i in range(len(queryImageList)):
        # get a query image
        query_image = queryImageList[i]
        # get query image id
        query_image_id = __getIdFromFilePath(queryImageNameList[i], True)
        # calculate shortest distance
        shortestDistanceImages = calculateQueryHistogramAndDistances(query_image, feature_method, N_Retrieval, False)
        found_list = []
        found_count = 0.0
        # calculate MAP of one image
        for j in range(len(shortestDistanceImages)):
            path = shortestDistanceImages[j]
            shortest_image_id = __getIdFromFilePath(path, False)
            if query_image_id == shortest_image_id:
                found_count += 1.0
                found_list.append(found_count / (j + 1))
        if len(found_list) > 0:
            MAP.append(np.mean(found_list))
        else:
            # no match
            MAP.append(0)
    if len(MAP) > 0:
        return np.mean(MAP)
    else:
        # no match
        return 0


def img_retrieve(dataset_path, query_image, feature_method):
    """
    Retrieves all images from dataset path and
    ordered by feature method histogram min distances.

    :param dataset_path: dataset path that has image files
    :param query_image: any image path
    :param feature_method: RGB, GABOR or YOURS (my own feature)
    """
    util.writeLog("MAIN-PROCESS", "Image Retrieval program is started with " + feature_method)

    featureFactory(dataset_path, feature_method)
    calculateQueryHistogramAndDistances(cv2.imread(query_image), feature_method, -1, True)  # -1 means get all images

    util.writeLog("MAIN-PROCESS", "Image Retrieval program is completed successfully.")


def mapCalculation(dataset_path, feature_method, N_Retrieval):
    """
    Calculates MAP value from QUERY_IMAGES folder.

    :param dataset_path: dataset path that has image files
    :param feature_method: RGB, GABOR or YOURS (my own feature)
    :param N_Retrieval: num of distance value
    """
    util.writeLog("MAIN-PROCESS", "MAP Calculation program is started with " + feature_method)

    featureFactory(dataset_path, feature_method)
    map_value = calculateMap(feature_method, N_Retrieval)
    util.writeLog("MAP", "Map is calculated for " + feature_method +
                  "  N: " + str(N_Retrieval) +
                  "   MAP: %" + str(map_value * 100))
    print(map_value)

    util.writeLog("MAIN-PROCESS", "MAP Calculation program is completed successfully.")


def main(argv):
    util.handlePart2Arguments(argv)
    if argv[1] == "QUERY":
        img_retrieve(argv[2], argv[3], argv[4])
    elif argv[1] == "MAP":
        mapCalculation(argv[2], argv[3], int(argv[4]))
    else:
        print("Please select 'QUERY' or 'MAP' type for program.")


if __name__ == "__main__":
    main(sys.argv)
