#!/bin/sh

# Part 1 for 3 dimension reduce
python2.7 ./part1.py ./Part1-dataset/ 3



# Part 2 MAP calculation for three feature with 10 retrieval
python2.7 ./part2.py MAP ./Part2-dataset/ color_hist 10

python2.7 ./part2.py MAP ./Part2-dataset/ gabor 10

python2.7 ./part2.py MAP ./Part2-dataset/ canny 10



# Part 2 Image Retrieval for three feature with query.png
python2.7 ./part2.py QUERY ./Part2-dataset/ ./query.png canny

python2.7 ./part2.py QUERY ./Part2-dataset/ ./query.png gabor

python2.7 ./part2.py QUERY ./Part2-dataset/ ./query.png color_hist